# EPAM_University_...Python_External..._
### [Lecture 1](/lecture_1/README.md) / Hometask 2

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

#### Task:
- [x] Calculate the area of triangle with sides a = 4.5, b = 5.9 and c = 9<br>
- [x] Print calculated area with precision = 2

#### Decision:
For the decision was used [Heron's formula](https://en.wikipedia.org/wiki/Heron%27s_formula) that gives the area of a triangle when the length of all three sides are known.

Defining **calculate_triangles_area** func:
```python
def calculate_triangles_area(side_a, side_b, side_c):
    """Calculates triangle's area with Heron's formula"""
    from math import sqrt

    # testing if triangle (2 sides are always bigger than one)
    sorted_sides = sorted([side_a, side_b, side_c])
    if sorted_sides[2] > sorted_sides[0] + sorted_sides[1]:
        raise Exception('That\'s not a triangle!')

    semi_perim = (side_a + side_b + side_c) / 2  # evals semi-perimeter
    area = sqrt(
        (semi_perim - side_a)
        * (semi_perim - side_b)
        * (semi_perim - side_c)
        * semi_perim
    )

    return area

```
Testing is made because the triangle inequality theorem states that for any triangle, the sum of the lengths of any two sides must be greater than or equal to the length of the remaining side. [Triangle inequality theorem](https://en.wikipedia.org/wiki/Triangle_inequality)

'Math' module was imported inside of the func's scope so it could be reused in other apps without calling it out of the funcs scope.

Printing and rounding result with precision = 2: 
```python
triangles_area = calculate_triangles_area(4.5, 5.9, 9)
print(round(triangles_area, 2))
```
Round() func was used outside of the **calculate_triangles_area** func in case user needs another output precision.

**Result:** 11.58

#### PS:
The code was checked in the http://pep8online.com/checkresult app. 
Check results showed up the **W503 mistake**. `pycodestyle (pep8)` linter (that I've installed into VSC) shows no mistakes.

The app is outdated. This rule goes against the PEP 8 recommended style, which was changed on April 16th, 2016 [source](https://www.flake8rules.com/rules/W503.html):

> For decades the recommended style was to break after binary operators. But this can hurt readability in two ways: the operators tend to get scattered across different columns on the screen, and each operator is moved away from its operand and onto the previous line. Here, the eye has to do extra work to tell which items are added and which are subtracted:

> To solve this readability problem, mathematicians and their publishers follow the opposite convention. Donald Knuth explains the traditional rule in his Computers and Typesetting series: "Although formulas within a paragraph always break after binary operations and relations, displayed formulas always break before binary operations".

Following the tradition from mathematics usually results in more readable code:
```python
# Correct:
# easy to match operators with operands
income = (gross_wages
          + taxable_interest
          + (dividends - qualified_dividends)
          - ira_deduction
          - student_loan_interest)
```
[Source 1](https://www.python.org/dev/peps/pep-0008/#should-a-line-break-before-or-after-a-binary-operator); [Source 2](https://pep8.org/#break-before-or-after-binary-operator)

### [Back to Lecture 1 folder](/lecture_1/README.md)
### [Back to Main Page](/README.md)