def calculate_triangles_area(side_a, side_b, side_c):
    """Calculates triangle's area with Heron's formula"""
    from math import sqrt

    # testing if triangle (2 sides are always bigger than one)
    sorted_sides = sorted([side_a, side_b, side_c])
    if sorted_sides[2] > sorted_sides[0] + sorted_sides[1]:
        raise Exception('That\'s not a triangle!')

    semi_perim = (side_a + side_b + side_c) / 2  # evals semi-perimeter
    area = sqrt(
        (semi_perim - side_a)
        * (semi_perim - side_b)
        * (semi_perim - side_c)
        * semi_perim
    )

    return area


triangles_area = calculate_triangles_area(4.5, 5.9, 9)
print(round(triangles_area, 2))
