# [EPAM_University_...Python_External..._](/README.md)
### Lecture 1

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task List:
- [x] Read PEP8 Style Guide
- [x] Calculate the area of triangle with sides a = 45, b = 5.9, c = 9. Print calculated value with precision = 2 

## Completed tasks:
### Task 1
1.1 Finished reading PEP8 [source 1](https://pep8.org/), [source 2](https://www.python.org/dev/peps/pep-0008/)<br>
1.2 Installed pylint and pycodestyle (pep8) into the Visual Studio Code<br>
        - [After pylint and pep8 installed (mistakes testing)](https://drive.google.com/file/d/10QImc65JRxP7D03rbjvvt3lx9OYlfEUj/view?usp=sharing)<br>
        - [After checking mistakes and warnings](https://drive.google.com/file/d/1sNP1nk2exKoWZPS1EDD3eB6EGC6yQTUO/view?usp=sharing)<br>

References: [VSC instructions](https://code.visualstudio.com/docs/python/linting#_specific-linters), 
[Dev.to instructions](https://dev.to/j0nimost/setting-up-pep8-and-pylint-on-vs-code-34h)

### Task 2
[1. Follow HomeTask 2 (README.md)](/lecture_1/HomeTask_2/README.md)


### [Back to Main Page](/README.md)
