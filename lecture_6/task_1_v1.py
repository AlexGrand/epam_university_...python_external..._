from typing import Tuple


def combine_dicts(*args: Tuple[dict, ...]) -> dict:
    from collections import Counter
    counter = Counter()

    for dc in args:
        counter.update(dc)
    return dict(counter)


if __name__ == "__main__":
    dict_1 = {'a': 100, 'b': 200}
    dict_2 = {'a': 200, 'c': 300}
    dict_3 = {'a': 300, 'd': 100}

    print(
        f"For {dict_1} and {dict_2} combined dict = "
        + f"{combine_dicts(dict_1, dict_2)}"
        )
    print(
        f"For {dict_1} and {dict_2} and {dict_3 } combined dict = "
        + f"{combine_dicts(dict_1, dict_2, dict_3)}"
        )
