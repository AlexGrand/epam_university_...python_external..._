class Node:
    def __init__(self, element, nxt=None, prev=None):
        self.data = element
        self.next = nxt
        self.prev = prev


class CustomList:
    def __init__(self, *args):
        self.head = None
        self.tail = None
        self.length = 0
        if args:
            node = Node(args[0])
            self.head = node
            self.length += 1

            for el in args[1:]:
                node.next = Node(el, prev=node)
                node = node.next
                self.length += 1
            self.tail = node

    def __iter__(self):
        node = self.head
        while node is not None:
            yield node
            node = node.next

    def __repr__(self):
        if self.head is None:
            return '[]'
        string = ''
        for node in iter(self):
            string += str(node.data) + ' -> '

        return f"[{string} None]"

    def __getitem__(self, index):
        if type(index) is not int:
            raise TypeError(
                f"list indices must be integers, not {type(index)}"
                )
        elif not 0 <= index < self.length:
            raise IndexError(
                f"llist index out of range"
            )

        if index == 0:
            return self.head.data
        elif index == self.length - 1:
            return self.tail.data

        cases = {
            True: {
                'i': 0, 'node': self.head, 'step': 1, 'next': 'next'
            },
            False: {
                'i': self.length - 1, 'node': self.tail, 'step': -1,
                'next': 'prev'
            }
        }

        opt = cases[index < self.length // 2]

        i = opt['i']
        node = opt['node']
        while i != index:
            node = node.__getattribute__(opt['next'])
            i += opt['step']
        return node.data

    def __setitem__(self, index, val):
        if type(index) is not int:
            raise TypeError(
                f"list indices must be integers, not {type(index)}"
                )
        elif not 0 <= index < self.length:
            raise IndexError(
                f"llist index out of range"
            )

        if self.length == 1 or index == 0:
            self.head.data = val
            return
        elif index == self.length - 1:
            self.tail.data = val
            return

        cases = {
            True: {
                'i': 0, 'node': self.head, 'step': 1, 'next': 'next'
            },
            False: {
                'i': self.length - 1, 'node': self.tail, 'step': -1,
                'next': 'prev'
            }
        }
        opt = cases[index < self.length // 2]

        i = opt['i']
        node = opt['node']
        while i != index:
            node = node.__getattribute__(opt['next'])
            i += opt['step']
        node.data = val
        return

    def __delitem__(self, index):
        if type(index) is not int:
            raise TypeError(
                f"list indices must be integers, not {type(index)}"
                )
        elif not 0 <= index < self.length:
            raise IndexError(
                f"llist index out of range"
            )

        if self.length == 1:
            self.head = None
            self.tail = None
            self.length = 0
            return
        elif index == 0:
            self.head = self.head.next
            self.head.prev = None
            self.length -= 1
            return
        elif index == self.length-1:
            self.tail = self.tail.prev
            self.tail.next = None
            self.length -= 1
            return

        cases = {
            True: {
                'i': 0, 'node': self.head, 'step': 1, 'next': 'next'
            },
            False: {
                'i': self.length - 1, 'node': self.tail, 'step': -1,
                'next': 'prev'
            }
        }
        opt = cases[index < self.length // 2]
        i = opt['i']
        node = opt['node']
        while i != index:
            node = node.__getattribute__(opt['next'])
            i += opt['step']
        node.prev.next = node.next
        node.next.prev = node.prev
        self.length -= 1
        return

    def __len__(self):
        return self.length

    def append(self, val):
        if self.length == 0:
            node = Node(val)
            self.head = node
            self.tail = node
            self.length += 1
            return
        else:
            node = Node(val, prev=self.tail, nxt=None)
            self.tail.next = node
            self.tail = node
            self.length += 1
            return

    def prepend(self, val):
        if self.length == 0:
            node = Node(val)
            self.head = node
            self.tail = node
            self.length += 1
            return
        else:
            node = Node(val, prev=None, nxt=self.head)
            self.head.prev = node
            self.head = node
            self.length += 1
            return

    def index(self, val):
        for i, node in enumerate(iter(self)):
            if node.data == val:
                return i
        raise ValueError(f"'{val}' is not in llist!")

    def insert(self, val, index=0):
        if type(index) is not int:
            raise TypeError(
                f"list indices must be integers, not {type(index)}"
                )
        elif index == 0 and self.length == 0:
            self.append(val)
            return
        elif not 0 <= index < self.length:
            raise IndexError(
                f"llist index out of range"
            )

        if index == 0:
            self.prepend(val)
            return
        elif index == self.length - 1:
            self.append(val)
            return

        cases = {
            True: {
                'i': 0, 'node': self.head, 'step': 1, 'next': 'next'
            },
            False: {
                'i': self.length - 1, 'node': self.tail, 'step': -1,
                'next': 'prev'
            }
        }
        opt = cases[index < self.length // 2]
        i = opt['i']
        node = opt['node']
        while i != index:
            node = node.__getattribute__(opt['next'])
            i += opt['step']
        new_node = Node(val, prev=node.prev, nxt=node)
        node.prev.next = new_node
        node.prev = new_node
        self.length += 1
        return

    def remove(self, val):
        if not val:
            raise TypeError("remove() takes exactly one argument")
        elif self.length == 0:
            raise ValueError("llist.remove(x): x not in llist")

        if self.length == 1 or self.head.data == val:
            self.__delitem__(0)
            return
        elif self.tail.data == val:
            self.__delitem__(self.length-1)
            return

        for node in iter(self):
            if node.data == val:
                node.prev.next = node.next
                node.next.prev = node.prev
                self.length += 1
                return
        raise ValueError("llist.remove(x): x not in llist")

    def clear(self):
        self.head = None
        self.tail = None
        self.length = 0
        return self


if __name__ == "__main__":
    msg = "Task 1: Creating empty user list:\n"
    llist = CustomList()
    print(msg)
    print("llist=CustomList()\nllist =", llist)

    msg = "\nTask 2: Create list based on elements set (the elements are stored in "
    msg += "CustomList in form of undirectional linked list:\n"
    print(msg)
    llist = CustomList(1, {'a': 'a'}, ['apple'], 3)
    print("llist = CustomList(1, {'a': 'a'}, ['apple'], 3):\n", llist)

    msg = "\nTask 3: adding, removing elements:\n"
    print(msg)
    del llist[1]
    print("del llist[1]:\n", llist)
    llist.remove(['apple'])
    print("\nllist.remove(['apple']:\n", llist)
    llist.append('last')
    print("\nllist.append('last'):\n", llist)
    llist.prepend('first')
    print("\nllist.prepend('first'):\n", llist)
    llist.insert('third', 2)
    print("\nllist.insert('third', 2):\n", llist)

    msg = "\nTask 4: Operations with elements by index:\n"
    print(msg)
    print("llist[2]:\n", llist[2])
    llist[2] = [3, 'third']
    print("\nllist[2] = [3, 'third]:\n", llist)
    i = llist.index('last')
    print("\nllist.index('last'):\n", i)

    msg = "\nTask 5: Clearing the list, recieving its length:\n"
    print(msg)
    print("llist.length, len(llist):\n", llist.length, len(llist))
    print("\nllist.clear().length:\n", llist.clear().length)

    msg = "\nTask 6: Recieving link to linked elements list:\n"
    print(msg)
    ll1 = llist
    print("ll1 = llist, type(ll1), ll1 == llist\n", type(ll1), ll1 == llist)

    print("\nAdvanced level tasks require implementation of the following:")
    msg = "\nTask 7: All completed tasks of Low level: DONE!"
    print(msg)

    msg = "\nTask 9: Recieving from numerator list for operator for:\n"
    print(msg)
    print(
        """
        llist = CustomList(1, {'a': 'a'}, ['apple'], 3)
        for el in llist:
            print(el.data)
        """
    )
    llist = CustomList(1, {'a': 'a'}, ['apple'], 3)
    for el in llist:
        print(el.data)
