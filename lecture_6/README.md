# [EPAM_University_...Python_External..._](/README.md)
### Lecture 6

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task 1:
- [x] Create function `combine_dicts(*args)`
![combine_dicts](static/Task_1.png)

[Version 1](task_1_v1.py):

Func uses `collections.Counter` to save and update each iteration with increasing values
```python
def combine_dicts(*args: Tuple[dict, ...]) -> dict:
    from collections import Counter
    counter = Counter()

    for dc in args:
        counter.update(dc)
    return dict(counter)
```
**OUTPUT**
![output](static/Task1_output.png)

[Version 2](task_1_v2.py):

Func uses `functools.reduce` to accumulate dict values in empty dict. This method is more memory and time efficient:
```python
def combine_dicts(*args: Tuple[dict, ...]) -> dict:
    from functools import reduce

    def reduce_dicts(acc: dict, dic: dict) -> dict:
        for key, val in dic.items():
            acc[key] = acc.get(key, 0) + val
        return acc

    return reduce(reduce_dicts, list(args), {})
```
**OUTPUT**
![output](static/Task1_output.png)

## Task 2:
- [x] Create `CustomList`

![combine_dicts](static/Task_2.1.png)

[Version 1](task_2_v1.py):

Was created double linked list that has both `head` and `tail` pointers to the list. Each node also has a reference to the previous and next element in the linked list.
The `Node` looks like:
```python
class Node:
    def __init__(self, element, nxt=None, prev=None):
        self.data = element
        self.next = nxt
        self.prev = prev
```
The `CustomList` has `head`, `tail`, and `length` that is changed with every operation on this linked list. That way `len()` has O(1) time complexity.
Methods that use `index` can decide from wich side of the list to start iteration. The decision of starting from `head` or `tail` depends on evaluation of `self.length // 2`, that makes a search faster because of dividing linked list on half and telling which way is closer to look for a needed `index`
`CustomList`:
```python
class CustomList:
    def __init__(self, *args):
        self.head = None
        self.tail = None
        self.length = 0
        if args:
            node = Node(args[0])
            self.head = node
            self.length += 1

            for el in args[1:]:
                node.next = Node(el, prev=node)
                node = node.next
                self.length += 1
            self.tail = node
```
**TASKS:**<br>

![tasks](static/Task_2.2.png)

**OUTPUT**<br>
![tasks](static/t_1.png)

![tasks](static/t_2.png)

![tasks](static/t_3.png)

![tasks](static/t_4.png)

![tasks](static/t_5.png)

![tasks](static/t_6.png)

![tasks](static/t_7.png)

**TASK**
- [x] Task 8: Generate exception in methods:

All methods that have `index` param (getitem, setitem, delitem, index, insert) have simple check for the type of `index` (`int`) and if the `index` is not out of range:
```python
def __getitem__(self, index):
        if type(index) is not int:
            raise TypeError(
                f"list indices must be integers, not {type(index)}"
                )
        elif not 0 <= index < self.length:
            raise IndexError(
                f"llist index out of range"
            )
```
The `remove` method has exceptions for the cases if value wasn't given to the method or if seeked value is not in llist:
```python
if not val:
            raise TypeError("remove() takes exactly one argument")
        elif self.length == 0:
            raise ValueError("llist.remove(x): x not in llist")
```

![tasks](static/t_9.png)


### [Back to Main Page](/README.md)
