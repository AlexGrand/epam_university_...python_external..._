from typing import Callable, Tuple, Any


def remember_result(func: Callable) -> Callable:
    """Decorator function that takes func and arguments for it.
       Caches previous results of given func and prints it out"""
    __arguments = None

    def wrapper(*args: Tuple[Any, ...]) -> Any:
        nonlocal __arguments

        print(f"Last result = '{__arguments}'")
        __arguments = func(*args)
        return __arguments
    return wrapper


@remember_result
def sum_list(*args: Tuple[Any, ...]) -> str:
    """Function that converts all given values and concatenates them"""
    result = ""
    for item in args:
        result += str(item)

    print(f"Current result = '{result}'")
    return result


if __name__ == "__main__":
    print("\n")
    print('sum_list("a", "b")')
    sum_list("a", "b")
    print('sum_list("abc", "cde")')
    sum_list("abc", "cde")
    print('sum_list(3, 4, 5)')
    sum_list(3, 4, 5)
    print("\n")
