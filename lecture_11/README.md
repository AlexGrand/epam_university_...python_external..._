# [EPAM_University_...Python_External..._](/README.md)
### Lecture 10

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task 1:

![Task 1](static/Task_1.png)

**1)** To get the `inner_function` was implemented `return` for `enclosing_function`. `inner_function` is called using simple snippet:
```python
from modules import legb


def call_inner_from_enclosed():
    return legb.enclosing_funcion()

call_inner_from_enclosed()()
```

![input_1.1](static/input_1.1.png)

**output:**<br>  ![output_1.1](static/output_1.1.png)

**2)** Modify inner func to print 'a' from global scope:
![input_1.2](static/input_1.2.png)

**output:**<br>  ![output_1.2](static/output_1.2.png)

**3)** Modify inner func to print 'a' from enclosing function:
![input_1.3](static/input_1.3.png)

**output:**<br>  ![output_1.3](static/output_1.3.png)

## Task 2:

![Task 2](static/Task_2.png)

To cashe previous result in decorator func was implemented local variable `__arguments` which is called from `wrapper` func:
```python
def remember_result(func: Callable) -> Callable:
    """Decorator function that takes func and arguments for it.
       Caches previous results of given func and prints it out"""
    __arguments = None

    def wrapper(*args: Tuple[Any, ...]) -> Any:
        nonlocal __arguments

        print(f"Last result = '{__arguments}'")
        __arguments = func(*args)
        return __arguments
    return wrapper
```
Fuction `sum_list` is wrapped with `@remember_result` decorator:
```python
@remember_result
def sum_list(*args: Tuple[Any, ...]) -> str:
    """Function that converts all given values and concatenates them"""
    result = ""
    for item in args:
        result += str(item)

    print(f"Current result = '{result}'")
    return result
```
**OUTPUT**<br>
![output 2](static/output_2.png)

## Task 3:

![Task 3](static/Task_3.png)
```python
def call_once(func: Callable) -> Callable:
    """Decorator function that that runs given function only once.
       Caches first result and return only first result."""
    __arguments = None

    def wrapper(*args: Tuple[Any, ...]) -> Any:
        """Wrapper that takes any args from given function but returns
           only first result of it's call."""
        nonlocal __arguments

        result = func(*args)

        if __arguments is None:
            __arguments = result

        return __arguments
    return wrapper
```
`call_once` decorator wraps `sum_of_numbers` func and cashes only first result of it's call.
```python
@call_once
def sum_of_numbers(a: int, b: int) -> int:
    """Summarize two values and returns result (int).

    Params:
    a (int): must be int

    b (int): must be int"""
    return int(a) + int(b)
```
**OUTPUT:**<br>
![output 3](static/output_3.png)

## Task 4:

![Task 4](static/Task_4.png)

According to the py docs `import` statement initialize all statements that happen in the imported module:

![output 4](static/output_4.png)

`mod_a`:
```python
import mod_c
import mod_b


print(mod_c.x)
```
`mod_b`
```python
import mod_c

mod_c.x = 5
```
`mod_c`:
```python
x = [1, 2, 3]
```

When `mod_c` is imported the variable `x` is intialised in global scope. When `mod_b` is imported the `mod_b` changes `x` variable in `mod_c`.
Same happens if we are using `from mod import *` statement. The only difference is `import <module>` imports the whole module and inner variables are accessed through module.variable notation. And `*` statement imports all variables into local scope of the running script, which can mess local variables.

To prevent `mod_b` changing `x` variable during import of the module - we can use construction that executes module statement only if the concrete script is fired up.

`mod_b`:
```python
import mod_c

if __name__ ==" __main__":
    mod_c.x = 5
```

### [Back to Main Page](/README.md)