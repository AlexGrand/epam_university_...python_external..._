from modules import legb


def call_inner_from_enclosed():
    return legb.enclosing_funcion()


if __name__ == "__main__":
    print("\n")
    print(call_inner_from_enclosed()(), "\n")
