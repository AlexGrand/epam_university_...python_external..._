from typing import Callable, Any, Tuple


def call_once(func: Callable) -> Callable:
    """Decorator function that that runs given function only once.
       Caches first result and return only first result."""
    __arguments = None

    def wrapper(*args: Tuple[Any, ...]) -> Any:
        """Wrapper that takes any args from given function but returns
           only first result of it's call."""
        nonlocal __arguments

        result = func(*args)

        if __arguments is None:
            __arguments = result

        return __arguments
    return wrapper


@call_once
def sum_of_numbers(a: int, b: int) -> int:
    """Summarize two values and returns result (int).

    Params:
    a (int): must be int

    b (int): must be int"""
    return int(a) + int(b)


if __name__ == "__main__":
    print("\nprint(sum_of_numbers(13, 42))")
    print(">>>", sum_of_numbers(13, 42))

    print("\nprint(sum_of_numbers(999, 100))")
    print(">>>", sum_of_numbers(999, 100))

    print("\nprint(sum_of_numbers(134, 412))")
    print(">>>", sum_of_numbers(134, 412))

    print("\nprint(sum_of_numbers(856, 232))")
    print(">>>", sum_of_numbers(856, 232))
