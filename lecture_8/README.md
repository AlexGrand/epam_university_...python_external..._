# [EPAM_University_...Python_External..._](/README.md)
### Lecture 8

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task 1:

![Task 1](static/Task_1.png)

**Output**
If input `a` is a number that can be converted into `int`:
*if a is less or equal to b the output will raise `AssertionError` with text `Not enough`
*if a is greater than b, there will be no output

if `a` can't be converted into `int` type `ValueError` will be raised.

## Task 2:

![Task 2](static/Task_2.png)

`foo` func will print `finally` block with output 'after bar' and then raise `NameError`: name 'bar' is not defined

## Task 3:

![Task 3](static/Task_3.png)

`baz` func will run `finally` block and return 2. Output will print 2

## Task 4:

![Task 4](static/Task_4.png)

`foo` func will raise `SyntaxError` cause `finally` clause must be last in `try...finally` compound statement.

## Task 5:

![Task 5](static/Task_5.png)

Because `'1' != 1` is True the `if` clause will execute and raise `Error`. `TypeError` will be fired cause exceptions must inherit from BaseException class. Next `except` clause will run. `except` clause will raise `TypeError` because it will try to catch error that doesn't inherit from BaseException. 

## Task 6:

![Task 6](static/Task_6.png)

This code snippet will work endlessly or till memory overfilled. In case file doesn't exist the output will be 'Input file not found' and cycle starts again. In case file exists it will open the file in 'r' mode and start again. On each step it will overwrite `filename` variable. 

### [Back to Main Page](/README.md)
