import sys
import os


def most_common_words_in_fl(fl_path: str, w_num: int = 3) -> list:
    """Gets most common words in file and return list with w_num length"""
    if not fl_path:
        raise ValueError("'fl_path' can't be empty")
    elif type(fl_path) is not str:
        raise ValueError("fl_path must be str")

    from re import sub
    from collections import Counter

    try:
        if not os.path.exists(fl_path):
            raise FileNotFoundError
    except FileNotFoundError:
        print("\n")
        print(f"Path: {fl_path} doesn't exist\n")
        exit()

    words_dict = Counter()
    with open(fl_path, encoding="utf-8") as fl:
        for line in fl:
            if line == '\n':
                continue
            line = line.lower().split()
            words_dict.update(sub(r"[^0-9a-zA-Z]", "", word) for word in line)

    return words_dict.most_common(w_num)


if __name__ == "__main__":
    fl_name = "lorem_ipsum.txt"
    path = os.path.join(f"{sys.path[0]}", "data", fl_name)

    print(f"\nMost common words in '{fl_name}' file are:\n")
    print(most_common_words_in_fl(path))
