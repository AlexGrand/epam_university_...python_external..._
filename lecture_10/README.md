# [EPAM_University_...Python_External..._](/README.md)
### Lecture 10

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task 1:

![Task 1](static/Task_1.png)

Firstly func `sort_fl_to_new_fl` uses simple check of input fl_names:
```python
if not fl_name or not new_fl_name:
        raise ValueError("'fl_name' and 'new_fl_name' can't be empty")
    elif type(fl_name) is not str or type(new_fl_name) is not str:
        raise ValueError("fl_name and new_fl_name must be str")
```
Then path is checked in try block. If path doesn't exist program throws exception and stops.

Path is created dynamically relative to the script location not to the directory it was called from by interpreter. 

```python
from sys import path as sys_path
    from os import path as os_path

    try:
        path = os_path.join(f"{sys_path[0]}", "data", fl_name)
        if not os_path.exists(path):
            raise FileNotFoundError
    except FileNotFoundError:
        print("\n")
        print(f"Path: {path} doesn't exist\n")
        exit()
```
If path exists file is opened and read line by line and sorted afterwards.
```python
fl_lines = []

    with open(path, encoding="utf-8") as fl:
        for line in fl:
            fl_lines.append(line)

    unsorted_lines = fl_lines
    sorted_lines = sorted(unsorted_lines)
```
Next path for new file is created and sorted_lines are writtend into new file.

```python
new_fl_path = os_path.join(f"{sys_path[0]}", "data", new_fl_name)

    with open(new_fl_path, 'w', encoding="utf-8") as new_fl:
        for line in sorted_lines:
            new_fl.write(line)

    return unsorted_lines, sorted_lines
```

**OUTPUT**<br>
![output 1](static/output_1.png)

## Task 2:

![Task 2](static/Task_2.png)

`most_common_words_in_fl` func makes simple check on type of file path and if it is empty:
```python
if not fl_path:
        raise ValueError("'fl_path' can't be empty")
    elif type(fl_path) is not str:
        raise ValueError("fl_path must be str")
```
Func tries to find the file. If path doesn't exist it throws exception and stops execution.
```python
from re import sub
    from collections import Counter

    try:
        if not os.path.exists(fl_path):
            raise FileNotFoundError
    except FileNotFoundError:
        print("\n")
        print(f"Path: {fl_path} doesn't exist\n")
        exit()
```
Then words_dict Counter is created to hold all words and their count.
File is opened and checked line by line. Each line is lowered down, splited and each word is cleaned up from useless symbols using regular expressions.
```python
words_dict = Counter()
    with open(fl_path, encoding="utf-8") as fl:
        for line in fl:
            if line == '\n':
                continue
            line = line.lower().split()
            words_dict.update(sub(r"[^0-9a-zA-Z]", "", word) for word in line)

    return words_dict.most_common(w_num)
```
**OUTPUT**<br>
![output 2](static/output_2.png)


## Task 3:

![Task 3](static/Task_3.png)

CSVFile and Student classes are created to hold information about CSV file and each student. 
File is repacked with csv module. Sorted by Counters and then written in new file with csvDictReader. Sorted values are cached inside of class.

[task_3](task_3.py)

**OUTPUT**<br>
![output 3](static/output_3.png)

![output 3](static/output_3_1.png)

Added [task 3 v2](task_3_v2.py) for task 3
This version uses `sorted` func instead of `Counter`. Class doesn't depend on special fields in CSV file, can unpack all of them and sort any available field (previously sorting options were predefined in class). Class also supports caching for already sorted values. Basic sort is done in:
```python
def __sort_fields_by(self, order_by: str, desc: bool = False) -> list:
        if order_by not in self.__fields:
            raise ValueError(f"There is no '{order_by}' field in CSV file")
        elif type(desc) is not bool:
            raise ValueError("'desc' param must be True or False")

        sorted_ls = self.__sorted_by.get(f"{order_by}_{desc}")

        if not sorted_ls:
            dct = self.__students
            sorted_ls = sorted(
                dct,
                key=lambda x: float(dct.get(x).get(order_by)),
                reverse=desc)
            self.__sorted_by[f"{order_by}_{desc}"] = sorted_ls

        return sorted_ls
```

Added [task 3 v2](task_3_v3.py) for task 3
`CSVFile` only conains only methods for reading CSV file, writing new one and sorting existiong one. CSVFile uses csv.DictReader and csv.DictWriter methods
Function `get_top_performers` is defined outside of class scope.

### [Back to Main Page](/README.md)
