import os
import sys
import csv


class CSVFile:
    def __init__(self):
        """Creates CSVFile object from any .csv file"""
        self.__items = []
        self.__sorted_by = {}
        self.__fields = []

    def read(self, fl_path: str):
        """Reads .csv file and adds it data to the CSVFile object"""
        if not os.path.exists(fl_path):
            print("\n")
            raise FileNotFoundError(f"Path: {fl_path} doesn't exist\n")
        elif not fl_path.endswith('.csv'):
            raise ValueError("Given file is not a csv")

        with open(fl_path, encoding='utf-8') as fl:
            reader = csv.DictReader(fl)
            self.__fields = reader.fieldnames
            for row in reader:
                for field in self.__fields:
                    try:
                        el = row[field]
                        if el.isdigit():
                            row[field] = int(el)
                        else:
                            row[field] = float(el)
                    except ValueError:
                        row[field] = str(row[field])

                self.__items.append(row)

    def sort_by(self, order_by: str, asc: bool = True) -> list:
        """Sorts .csv file with given 'order_by' and in asc or desc order"""
        if order_by not in self.__fields:
            raise ValueError(f"'{order_by}' field is not in CSV fields")
        elif type(asc) is not bool:
            raise ValueError("'asc' param must be True or False")

        sorted_ls = self.__sorted_by.get(f"{order_by}_{asc}")

        if not sorted_ls:
            ls = self.__items
            sorted_ls = sorted(
                ls, key=lambda x: x[order_by], reverse=(not asc))
            self.__sorted_by[f"{order_by}_{asc}"] = sorted_ls

        return sorted_ls

    def write(self, fl_path: str, order_by: str, asc: bool = True):
        """Writes data into new .csv file with given sort order"""
        if not fl_path.endswith('.csv'):
            raise ValueError("Given file is not a csv")

        sorted_ls = self.sort_by(order_by, asc)

        with open(fl_path, 'w', encoding='utf-8') as fl:
            writer = csv.DictWriter(fl, fieldnames=self.__fields)
            writer.writeheader()

            print("\nWriting file...")
            print(f"Destination folder is: {fl_path}")

            for line in sorted_ls:
                writer.writerow(line)

            print("\n ...finished")


def get_top_performers(csv_obj: object, n: int = 5) -> list:
    """Gets top performers from given CSVFile with n length"""
    if not isinstance(csv_obj, CSVFile):
        ValueError(f"Given obj is not a CSVFile class")
    elif type(n) is not int:
        ValueError("n must be int")

    best_performers = csv_obj.sort_by('average mark', asc=False)
    ls_of_best_n = []

    for item in best_performers[:n]:
        ls_of_best_n.append(item['student name'])

    return ls_of_best_n


if __name__ == "__main__":
    csv_fl_name = 'students.csv'
    csv_fl_path = os.path.join(f"{sys.path[0]}", "data", csv_fl_name)
    csv_fl = CSVFile()
    csv_fl.read(csv_fl_path)

    print("\nBest students are:")
    print(get_top_performers(csv_fl))

    csv_new_fl_name = 'students_sorted_desc_by_age.csv'
    csv_new_fl_path = os.path.join(f"{sys.path[0]}", "data", csv_new_fl_name)
    csv_fl.write(csv_new_fl_path, 'age', asc=False)
