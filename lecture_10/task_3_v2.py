import os
import sys
import csv


class CSVFile:
    def __init__(self, fl_path: str):
        if not os.path.exists(fl_path):
            print("\n")
            raise FileNotFoundError(f"Path: {fl_path} doesn't exist\n")
        elif not fl_path.endswith('.csv'):
            raise ValueError("Given file is not a csv")

        self.__fields = []
        self.__students = {}
        self.__sorted_by = {}

        with open(fl_path, encoding='utf-8') as csv_fl:
            reader = csv.reader(csv_fl)
            self.__fields = next(reader)

            for i, row in enumerate(reader):
                self.__students[i] = {}
                for y, field in enumerate(self.__fields):
                    self.__students[i][field] = row[y]

    def __sort_fields_by(self, order_by: str, desc: bool = False) -> list:
        if order_by not in self.__fields:
            raise ValueError(f"There is no '{order_by}' field in CSV file")
        elif type(desc) is not bool:
            raise ValueError("'desc' param must be True or False")

        sorted_ls = self.__sorted_by.get(f"{order_by}_{desc}")

        if not sorted_ls:
            dct = self.__students
            sorted_ls = sorted(
                dct,
                key=lambda x: float(dct.get(x).get(order_by)),
                reverse=desc)
            self.__sorted_by[f"{order_by}_{desc}"] = sorted_ls

        return sorted_ls

    def get_students_by_mark(
            self,
            desc: bool = False,
            n: int = 5) -> list:

        if type(n) is not int:
            raise ValueError(f"'n' must be int")
        elif n >= len(self.__students):
            raise ValueError("n is out of range")

        sorted_ls = self.__sort_fields_by('average mark', desc)
        students_ls = []

        for student in sorted_ls[:n]:
            students_ls.append(self.__students[student][self.__fields[0]])

        return students_ls

    def get_students_by_age(
            self,
            desc: bool = False,
            n: int = 5) -> list:

        if type(n) is not int:
            raise ValueError(f"'n' must be int")
        elif n >= len(self.__students):
            raise ValueError("n is out of range")

        sorted_ls = self.__sort_fields_by('age', desc)
        students_ls = []

        for student in sorted_ls[:n]:
            students_ls.append(self.__students[student][self.__fields[0]])

        return students_ls

    def write_new_csv(
            self, fl_path: str,
            order_by: str,
            desc: bool = False):

        if order_by not in self.__fields:
            raise ValueError(f"'{order_by}' is not in CSV file fields")

        students_index_ls = self.__sort_fields_by(order_by, desc)

        with open(fl_path, 'w', encoding="utf-8") as csv_fl:
            writer = csv.DictWriter(csv_fl, fieldnames=self.__fields)

            msg = f"\nCreating CSV File with fields: {self.__fields}"
            msg += f" that is ordered by: {order_by}"
            print(msg)
            print(f"...file is located at '{fl_path}'")

            writer.writeheader()
            for studt_i in students_index_ls:
                row_dict = {}
                for field in self.__fields:
                    row_dict[field] = self.__students[studt_i][field]

                writer.writerow(row_dict)


if __name__ == "__main__":
    csv_fl_name = 'students.csv'
    csv_fl_path = os.path.join(f"{sys.path[0]}", "data", csv_fl_name)

    csv_fl = CSVFile(csv_fl_path)

    print("\nBest 5 performers in CSVFile are next students:")
    print(csv_fl.get_students_by_mark(desc=True))
    print("\nWorst 5 performers in CSVFile are next students:")
    print(csv_fl.get_students_by_mark())

    print("\nOldest 5 students in CSVFile are next students:")
    print(csv_fl.get_students_by_age(desc=True))
    print("\nYoungest 5 students in CSVFile are next students:")
    print(csv_fl.get_students_by_age())

    csv_new_fl_name = 'students_sorted_desc_by_age.csv'
    csv_new_fl_path = os.path.join(f"{sys.path[0]}", "data", csv_new_fl_name)

    print(
        f"\nPath to new file with students sorted by age in desc order:\n",
        csv_new_fl_path)
    print("\nCreating new file...")
    csv_fl.write_new_csv(csv_new_fl_path, order_by='age', desc=True)
    print("\n...Finish creating new file")
