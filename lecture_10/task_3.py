import os
import sys
from collections import Counter
import csv


class Student:
    def __init__(self, name: str, age: int, mark: float):
        self.__name = str(name)
        self.__age = int(age)
        self.__mark = float(mark)

    @property
    def name(self):
        return self.__name

    @property
    def age(self):
        return self.__age

    @property
    def mark(self):
        return self.__mark


class CSVFile:
    def __init__(self, fl_path: str):
        if not os.path.exists(fl_path):
            print("\n")
            raise FileNotFoundError(f"Path: {fl_path} doesn't exist\n")

        self.__fields = []
        self.__students = {}
        self.__perfom_counter = Counter()
        self.__age_counter = Counter()

        with open(fl_path, encoding='utf-8') as csv_fl:
            reader = csv.reader(csv_fl)
            self.__fields = next(reader)

            for row in reader:
                name, age, mark = row
                name_repeats = 0
                while self.__students.get(name):
                    name_repeats += 1
                    name = f"{name}{name_repeats}"

                self.__students[name] = Student(name, age, mark)

    def get_top_performers(
            self,
            top_perform_n: int = 5, asc: bool = True
            ) -> list:

        if type(top_perform_n) is not int:
            raise ValueError(f"top_perform_n must be int")
        elif type(asc) is not bool:
            raise ValueError(
                f"'asc' ascending param must be True or False")

        if not len(self.__perfom_counter) > 0:
            for name, stud_obj in self.__students.items():
                self.__perfom_counter.update({name: stud_obj.mark})

        if asc:
            top_performers = self.__perfom_counter.most_common(top_perform_n)
            return top_performers
        else:
            worst_performers = self.__perfom_counter.most_common()
            return worst_performers[:-(top_perform_n+1):-1]

    def get_by_age(
            self,
            n: int = 5, asc: bool = True) -> list:
        if type(n) is not int:
            raise ValueError(f"'n' must be int")
        elif type(asc) is not bool:
            raise ValueError(
                f"'asc' ascending param must be True or False")

        if not len(self.__age_counter) > 0:
            for name, stud_obj in self.__students.items():
                self.__age_counter.update({name: stud_obj.age})

        if asc:
            youngest = self.__age_counter.most_common()
            return youngest[:-(n+1):-1]
        else:
            oldest = self.__age_counter.most_common(n)
            return list(oldest)

    def write_new_csv(
            self, fl_path: str,
            order_by: str,
            asc: bool = True):

        if order_by == "age":
            students = self.get_by_age(len(self.__students), asc=asc)
        elif order_by == "marks" or order_by == "perfomance":
            students = self.get_top_performers(len(self.__students), asc=asc)
        else:
            raise ValueError("order_by can be 'age', 'marks', 'perfomance")

        with open(fl_path, 'w', encoding="utf-8") as csv_fl:
            writer = csv.DictWriter(csv_fl, fieldnames=self.__fields)

            msg = f"\nCreating CSV File with fields: {self.__fields}"
            msg += f" that is ordered by: {order_by}"
            print(msg)
            print(f"...file is located at '{fl_path}'")

            writer.writeheader()
            for student in students:
                name = self.__students[student[0]].name
                age = self.__students[student[0]].age
                mark = self.__students[student[0]].mark

                row_dict = {
                    'student name': name,
                    'age': age,
                    'average mark': mark}

                writer.writerow(row_dict)


if __name__ == "__main__":
    csv_fl_name = 'students.csv'
    csv_fl_path = os.path.join(f"{sys.path[0]}", "data", csv_fl_name)

    csv_fl = CSVFile(csv_fl_path)

    print("\nBest 5 performers in CSVFile are next students:")
    print(csv_fl.get_top_performers())
    print("\nWorst 5 performers in CSVFile are next students:")
    print(csv_fl.get_top_performers(asc=False))

    print("\nOldest 5 students in CSVFile are next students:")
    print(csv_fl.get_by_age(asc=False))
    print("\nYoungest 5 students in CSVFile are next students:")
    print(csv_fl.get_by_age())

    csv_new_fl_name = 'students_sorted_desc_by_age.csv'
    csv_new_fl_path = os.path.join(f"{sys.path[0]}", "data", csv_new_fl_name)

    print(
        f"\nPath to new file with students sorted by age in desc order:\n",
        csv_new_fl_path)
    print("\nCreating new file...")
    csv_fl.write_new_csv(csv_new_fl_path, order_by='age', asc=False)
    print("\n...Finish creating new file")
