def sort_fl_to_new_fl(fl_name: str, new_fl_name: str) -> list:
    """Gets name of unsorted file in data folder and creates new \
        file with sorted names. Returns unsorted and sorted lines"""
    if not fl_name or not new_fl_name:
        raise ValueError("'fl_name' and 'new_fl_name' can't be empty")
    elif type(fl_name) is not str or type(new_fl_name) is not str:
        raise ValueError("fl_name and new_fl_name must be str")

    from sys import path as sys_path
    from os import path as os_path

    try:
        path = os_path.join(f"{sys_path[0]}", "data", fl_name)
        if not os_path.exists(path):
            raise FileNotFoundError
    except FileNotFoundError:
        print("\n")
        print(f"Path: {path} doesn't exist\n")
        exit()

    fl_lines = []

    with open(path, encoding="utf-8") as fl:
        for line in fl:
            fl_lines.append(line)

    unsorted_lines = fl_lines
    sorted_lines = sorted(unsorted_lines)

    new_fl_path = os_path.join(f"{sys_path[0]}", "data", new_fl_name)

    with open(new_fl_path, 'w', encoding="utf-8") as new_fl:
        for line in sorted_lines:
            new_fl.write(line)

    return unsorted_lines, sorted_lines


if __name__ == "__main__":
    unsorted_ls, sorted_ls = sort_fl_to_new_fl(
        'unsorted_names.txt', 'sorted_names.txt')
    print("\nBefore:\n")
    print(unsorted_ls)
    print("\nAfter:\n")
    print(sorted_ls)
