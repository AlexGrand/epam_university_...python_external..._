from enum import Enum


class SortOrder(Enum):
    ASCENDING = 'ascending'
    DESCENDING = 'descending'
    ASC = 'ascending'
    DESC = 'descending'
    A = 'ascending'
    D = 'descending'


def isSorted(ls: list, order: str = 'ascending') -> bool:
    cases = {'ascending': ls, 'descending': ls[::-1]}
    ls = cases[order]

    return all(ls[i] <= ls[i + 1] for i in range(len(ls) - 1))


if __name__ == "__main__":
    arr = [9, 8, 7]
    order = input("Choose sort order (ascending, descending): ").lower()

    try:
        order = SortOrder[order.upper()].value
        print(
            f"{arr} is sorted in '{order}' sort order:", isSorted(arr, order)
            )
    except KeyError:
        print('Choose only ascending/descending sort order!')
