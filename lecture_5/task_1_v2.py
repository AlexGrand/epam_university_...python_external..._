def isSorted(ls: list, order: str = 'ascending') -> bool:
    cases = {'ascending': ls, 'descending': ls[::-1]}
    cases['asc'] = cases['ascending']
    cases['desc'] = cases['descending']
    cases['a'] = cases['ascending']
    cases['d'] = cases['descending']

    if order.lower() not in cases.keys():
        raise KeyError(
            "Use only ascending/descending/asc/desc/a/d sort order!"
        )

    ls = cases[order]

    for i, el in enumerate(ls[1:]):
        if el <= ls[i]:
            return False
    return True


if __name__ == "__main__":
    arr = [1, 2, 5, 0]
    order = input("Choose sort order (ascending, descending): ").lower()
    print(
        f"{arr} is sorted in '{order}' sort order:", isSorted(arr, order)
        )
