def multArithmeticElements(num: int, step: int, n: int) -> int:
    if n <= 1:
        return num

    return num * multArithmeticElements(num+step, step, n-1)


if __name__ == "__main__":
    num, step, n = int(input('num: ')), int(input('step: ')), int(input('n: '))
    print(
        f'For a1={num}, t={step}, n={n}'
        + f' multiplication equals to {multArithmeticElements(num, step, n)}'
        )
