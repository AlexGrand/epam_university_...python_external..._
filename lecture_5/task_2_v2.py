def isSorted(ls: list, order: str = 'ascending') -> bool:
    cases = {'ascending': ls, 'descending': ls[::-1]}
    cases['asc'] = cases['ascending']
    cases['desc'] = cases['descending']
    cases['a'] = cases['ascending']
    cases['d'] = cases['descending']

    if order.lower() not in cases.keys():
        raise KeyError(
            "Use only ascending/descending/asc/desc/a/d sort order!"
        )

    ls = cases[order]

    for i, el in enumerate(ls[1:]):
        if el <= ls[i]:
            return False
    return True


def transform_list(ls: list, order: str = 'asc') -> list:
    if isSorted(ls, order):
        return list(el + i for i, el in enumerate(ls[:]))
    return ls[:]


def print_transformed(ls: list, order: str = 'asc') -> None:
    new_ls = transform_list(ls, order)
    message = f"For {ls} and '{order}' sort order values in the array"
    cases = {1: ' do not change', 0: f' are changing to {new_ls}'}
    print(message + cases[int(ls == new_ls)])


if __name__ == '__main__':
    arr = [5, 17, 24, 88, 33, 2]
    arr1 = [15, 10, 3]

    print_transformed(arr, 'ascending')
    print_transformed(arr1, 'ascending')
    print_transformed(arr1, 'descending')
