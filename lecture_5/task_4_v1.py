def check_step(func):
    def wrapper(*args):
        num, step, alim = args
        if not (0 < step < 1):
            raise Exception('STEP must be greater than 0 and less than 1')
        return func(num, step, alim)
    return wrapper


@check_step
def SumGeometricElements(num: int, step: float, alim: int) -> float:
    if num <= alim:
        return 0
    return num + SumGeometricElements(num*step, float(step), alim)


if __name__ == "__main__":
    num = int(input('num: '))
    step = float(input('step: '))
    alim = int(input('alim: '))
    msg = f'For a progression, where a1={num}, and t={step},'
    msg += f' the sum of the first elements, greater than alim={alim},'
    msg += f' equals to {round(SumGeometricElements(num, step, alim))}'
    print(msg)
