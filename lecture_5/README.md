# [EPAM_University_...Python_External..._](/README.md)
### Lecture 5

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task 1:
- [x] Create function `isSorted`
![isSorted](static/Task_1.png)

[Version 1](task_1_v1.py):
`isSorted` func uses `generator expression` and builtins `all()` func to check created list if the array is sorted. Goes through all array. Always has O(n) speed.
```python
def isSorted(ls: list, order: str = 'ascending') -> bool:
    cases = {'ascending': ls, 'descending': ls[::-1]}
    ls = cases[order]

    return all(ls[i] <= ls[i + 1] for i in range(len(ls) - 1))
```
[Version 2](task_1_v2.py):
Function `isSorted` enumerates given array and than checks if element is bigger than previous one in the `for` loop. Iteration is stopped immediately if current element is greater than previous one. Has O(n) only in the worst case - if array is already sorted.
```python
def isSorted(ls: list, order: str = 'ascending') -> bool:
    ...
    ...
    for i, el in enumerate(ls[1:]):
            if el <= ls[i]:
                return False
        return True
```
`isSorted` func also has a builtin check for the `SortOrder` parameter, that raises `KeyError` in case if sortorder `str` is not in the `cases` dict.
```python
def isSorted(ls: list, order: str = 'ascending') -> bool:
    cases = {'ascending': ls, 'descending': ls[::-1]}
    cases['asc'] = cases['ascending']
    cases['desc'] = cases['descending']
    cases['a'] = cases['ascending']
    cases['d'] = cases['descending']

    if order.lower() not in cases.keys():
        raise KeyError(
            "Use only ascending/descending/asc/desc/a/d sort order!"
        )
    ...
    ...
```

## Task 2:
- [x] Create function `Transform`
![Transpose](static/Task_2.png)

[Version 1](task_2_v2.py):

Function `transform_list` usese func [`isSorted`](task_1_v2.py) to check if given array is sorted with the given sorting order parameter. If so function returns new list with each element replaced with the sum of the current element and its index. In other case returns copy of the given array.
```python
def transform_list(ls: list, order: str = 'asc') -> list:
    if isSorted(ls, order):
        return list(el + i for i, el in enumerate(ls[:]))
    return ls[:]
```
Results are printed out with `print_transformed` func.

## Task 3:
- [x] Create function `multArithmeticElements`
![multArithmeticElements](static/Task_3.png)

[Version 1](task_3_v1.py):

Function just uses simple recursion with adding `step` to the `num` on each round.
```python
def multArithmeticElements(num: int, step: int, n: int) -> int:
    if n <= 1:
        return num

    return num * multArithmeticElements(num+step, step, n-1)

```

## Task 4
- [x] Create function `SumGeometricElements`
![SumGeometricElements](static/Task_4.png)

[Version 1](task_4_v1.py)

`SumGeometricElements` function again uses recursion like previous one. Recursion is stopped when `num` is less or equal to `alim`
```python
def SumGeometricElements(num: int, step: float, alim: int) -> float:
    if num <= alim:
        return 0
    return num + SumGeometricElements(num*step, float(step), alim)
```
Because according to the conditions of task the geometric progression is decreasing there is a special check for the user's input for the `step` value (`0<step<1`). That check is completed with `@check_step` decorator:
```python
def check_step(func):
    def wrapper(*args):
        num, step, alim = args
        if not (0 < step < 1):
            raise Exception('STEP must be greater than 0 and less than 1')
        return func(num, step, alim)
    return wrapper


@check_step
def SumGeometricElements(num: int, step: float, alim: int) -> float:
    ...
    ...
```

### [Back to Main Page](/README.md)