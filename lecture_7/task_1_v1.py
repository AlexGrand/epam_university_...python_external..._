class Rectangle:
    __sideB = 5.0

    def __init__(self, a, b=__sideB):
        self.__sideA = float(a)
        self.__sideB = float(b)

    def get_sideA(self):
        return self.__sideA

    def get_sideB(self):
        return self.__sideB

    @property
    def area(self):
        return self.__sideA * self.__sideB

    @property
    def perimeter(self):
        return self.__sideA * 2 + self.__sideB * 2

    @property
    def is_square(self):
        return self.__sideA == self.__sideB

    def replace_sides(self):
        self.__sideA, self.__sideB = self.__sideB, self.__sideA


class ArrayRectangles:
    """Gets and array of Rectangle objects or an arbitrary amount of
    objects of type Rectangle.
    If th
    """
    def __init__(self, *args, n=10):
        self.__rectangle_array = []
        self.__max_length = n
        self.__length = 0

        if args:
            if type(args[0]) is list:
                args = args[0]

            for item in args:
                if isinstance(item, Rectangle):
                    self.__rectangle_array.append(item)
                    self.__length += 1

    def add_rectangle(self, rect):
        if isinstance(rect, Rectangle) and self.__length < self.__max_length:
            self.__rectangle_array.append(rect)
            self.__length += 1
            return True
        return False

    def number_max_area(self):
        if not self.__rectangle_array:
            return
        elif self.__length == 1:
            return 0

        index = 0

        for i, rect in enumerate(self.__rectangle_array[1:], 1):
            prev_rect = self.__rectangle_array[index]
            if rect.area > prev_rect.area:
                index = i
        return index

    def number_min_perimeter(self):
        if not self.__rectangle_array:
            return
        elif self.__length == 1:
            return 0

        index = 0
        for i, rect in enumerate(self.__rectangle_array[1:], 1):
            prev_rect = self.__rectangle_array[index]
            if rect.area > prev_rect.area:
                index = i
        return index

    def number_min_area(self):
        if not self.__rectangle_array:
            return
        elif self.__length == 1:
            return 0

        index = 0
        for i, rect in enumerate(self.__rectangle_array[1:], 1):
            prev_rect = self.__rectangle_array[index]
            if rect.area > prev_rect.area:
                index = i
        return index

    def number_square(self):
        if not self.__rectangle_array:
            return

        num = 0
        for rect in self.__rectangle_array:
            if rect.is_square:
                num += 1
        return num

    @property
    def length(self):
        return self.__length


if __name__ == "__main__":
    rect = Rectangle(3)
    print("rect = Rectangle(3)")
    print(
        "rect.get_sideA():",
        rect.get_sideA()
        )
    print(
        "rect.get_sideB():",
        rect.get_sideB()
        )
    print(
        "rect.area:",
        rect.area
        )
    print(
        "rect.perimeter:",
        rect.perimeter
        )
    print(
        "rect.is_square:",
        rect.is_square
        )
    print("rect.replace_sides()")
    rect.replace_sides()
    print(
        "rect.get_sideA():",
        rect.get_sideA()
        )
    print(
        "rect.get_sideB():",
        rect.get_sideB()
    )

    # Advanced level:

    print("\nAdvanced Level:\n")
    print(
        "\nCreate class ArrayRectangles that takes arbitrary amount",
        "of objects of type Rectangle:")
    arr1 = ArrayRectangles(Rectangle(1), Rectangle(3, 5))
    print("arr1 = ArrayRectangles(Rectangle(1), Rectangle(3, 5))")
    print("arr1.length = ", arr1.length)

    print(
        "\nCreate class ArrayRectangles that takes and array",
        "of objects of type Rectangle and has length n:")
    arr2 = ArrayRectangles([Rectangle(2, 5)], n=1)
    print("arr2 = ArrayRectangles(Rectangle(2, 5), n=1)")
    print("arr2.length = ", arr2.length)

    print(
        "\nMethod 'add_rectangle' that adds a rectangle of type",
        "'Rectangle' to the array on the nearest free place",
        "and return true or false, if there is no free space")
    print(
        "\nAdding rectangle to the arr1:\n",
        "arr1.add_rectangle(Rectangle(7, 2)):",
        arr1.add_rectangle(Rectangle(7, 2)))
    print("arr1.length =", arr1.length)

    print(
        "\nAdding rectangle to the arr2:\n",
        "arr2.add_rectangle(Rectangle(7, 2)):",
        arr2.add_rectangle(Rectangle(7, 2)))
    print("arr1.length =", arr2.length)

    print(
        "\nMethod number_max_area that returns index of the rectangle",
        "with the maximum area value:"
    )
    print("arr1.number_max_area():", arr1.number_max_area())

    print(
        "\nMethod number_min_perimeter, that returns index of the",
        "rectangle with the minimum area value"
    )
    print("arr1.number_min_perimeter():", arr1.number_min_perimeter())

    print(
        "\nMethod number_square that returns the number of squares",
        "in the array of rectangles"
    )
    print("arr1.number_square():", arr1.number_square())

    print(
        "\nMethod number_min_area, that returns index of the",
        "rectangle with the minimum area value"
    )
    print("arr1.number_min_area():", arr1.number_min_area())
