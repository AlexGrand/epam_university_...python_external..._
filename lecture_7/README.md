# [EPAM_University_...Python_External..._](/README.md)
### Lecture 7

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task 1:

![Create Classes](static/Task_1.png)

[Version 1](task_1_v1.py):

```python
class Rectangle:
    __sideB = 5.0

    def __init__(self, a, b=__sideB):
        self.__sideA = float(a)
        self.__sideB = float(b)

    def get_sideA(self):
        return self.__sideA

    def get_sideB(self):
        return self.__sideB

    def area(self):
        return self.__sideA * self.__sideB

    def perimeter(self):
        return self.__sideA * 2 + self.__sideB * 2

    def is_square(self):
        return self.__sideA == self.__sideB

    def replace_sides(self):
        self.__sideA, self.__sideB = self.__sideB, self.__sideA
```

**OUTPUT**<br>
![Output](static/Output_task1.png)

**ADVANCED**<br>

![Advanced](static/Task_1_advanced.png)

**OUTPUT**<br>
![Output](static/Output_task1_advanced.png))


## Task 2:

![Create Classes](static/Task_2.png)

[Version 1](task_2_v1.py)

**OUTPUT**<br>
![Output](static/Output_task2_advanced.png))

**ADVANCED**<br>

![Advanced](static/Task_2_advanced.png)

**OUTPUT**<br>

![Output](static/Output_task2_advanced_1.png))


### [Back to Main Page](/README.md)