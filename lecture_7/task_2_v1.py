class Employee:
    __basic_bonus = 0.0  # basic bonus for any employee

    def __init__(self, name, salary):
        self.__name = str(name)
        self.__salary = float(salary)
        self.__bonus = self.__basic_bonus
        self.__personal_bonus = 0.0

    @property
    def name(self):
        return self.__name

    @property
    def salary(self):
        return self.__salary

    @salary.setter
    def salary(self, salary):
        self.__salary = float(salary)

    @property
    def bonus(self):
        return float(self.__bonus)

    def set_bonus(self, bonus=None):
        if bonus:
            self.__personal_bonus = float(bonus)
            self.__bonus = self.__basic_bonus + float(bonus)

    def to_pay(self):
        return self.salary + self.bonus


class SalesPerson(Employee):
    def __init__(self, name, salary, percent):
        self.__percent = int(percent)
        Employee.__init__(self, name, salary)

    def set_bonus(self, bonus=None):
        if bonus:
            self._Employee__personal_bonus = float(bonus)
        self._Employee__bonus = self._Employee__personal_bonus
        self._Employee__bonus += Employee._Employee__basic_bonus

        if self.__percent > 200:
            self._Employee__bonus *= 3
        elif self.__percent > 100:
            self._Employee__bonus *= 2


class Manager(Employee):
    def __init__(self, name, salary, client_amount):
        self.__quantity = int(client_amount)
        super().__init__(name, salary)

    def set_bonus(self, bonus=None):
        if bonus:
            self._Employee__personal_bonus = float(bonus)
        self._Employee__bonus = self._Employee__personal_bonus
        self._Employee__bonus += Employee._Employee__basic_bonus

        if self.__quantity > 150:
            self._Employee__bonus += 1000
        elif self.__quantity > 100:
            self._Employee__bonus += 500


class Company:
    def __init__(self, *args):
        self.__employee_array = []

        if args:
            if type(args[0]) is list:
                args = args[0]
            for empl in args:
                if isinstance(empl, SalesPerson) or isinstance(empl, Manager):
                    self.__employee_array.append(empl)

    def give_everybody_bonus(self, company_bonus):
        Employee._Employee__basic_bonus = float(company_bonus)
        for empl in self.__employee_array:
            empl._Employee__bonus = 200
            empl.set_bonus()

    def total_to_pay(self):
        total = 0
        for empl in self.__employee_array:
            total += empl.to_pay()
        return total

    def name_max_salary(self):
        max_salary = 0
        max_salary_owner = None
        for empl in self.__employee_array:
            if empl.salary > max_salary:
                max_salary = empl.salary
                max_salary_owner = empl.name
        return max_salary_owner


def output(obj, name, salary, kpi, bonus):
    msg = ''
    if type(obj).__name__ == 'type':
        obj = obj(name, salary, kpi)
        msg += "Create "
    obj.set_bonus(bonus)
    kpi_descr = f"Now, because of {type(obj).__name__} {obj.name}'s kpi={kpi}"

    if isinstance(obj, SalesPerson):
        if kpi > 200:
            kpi_descr += f" his bonus is tripled: {obj.bonus}"
        elif kpi > 100:
            kpi_descr += f" his bonus is doubled: {obj.bonus}"
    elif isinstance(obj, Manager):
        if kpi > 150:
            kpi_descr += f" his bonus is increased by 1000: {obj.bonus}"
        elif kpi > 100:
            kpi_descr += f" his bonus is increased by 500: {obj.bonus}"

    print(
        f"\n{msg}{type(obj).__name__}",
        f"with name {obj.name},",
        f"salary {obj.salary},",
        f"and kpi {kpi}")
    print(f"Company's basic bonus is:", obj._Employee__basic_bonus)
    print(
        f"{type(obj).__name__} {obj.name} personal bonus:",
        f"{obj._Employee__personal_bonus}")
    print(
        f"Setting bonus for {obj.name} equal to",
        f"{obj._Employee__basic_bonus} + {obj._Employee__personal_bonus}")

    print(f"{kpi_descr}")
    print(f"{obj.name}'s salary to pay is:", obj.to_pay(), "\n")
    return obj


def company_output(company_bonus):
    alex_sales = output(SalesPerson, 'Alex', salary=12000, kpi=205, bonus=50)
    garry_manager = output(Manager, 'Garry', salary=12100, kpi=101, bonus=0)
    george_manager = output(Manager, 'George', salary=12500, kpi=152, bonus=30)

    company = Company(alex_sales, garry_manager, george_manager)
    company.give_everybody_bonus(company_bonus)
    print(f"**Now compamy gives everyone bonus equal to {company_bonus}**")

    for empl in company._Company__employee_array:
        name = empl.name
        salary = empl.salary
        bonus = empl._Employee__personal_bonus
        kpi = ''
        if type(empl).__name__ == 'SalesPerson':
            kpi = empl._SalesPerson__percent
        else:
            kpi = empl._Manager__quantity
        output(empl, name, salary, kpi, bonus)

    print(f"Company needs to pay in total: ", company.total_to_pay())
    print(
        f"The biggest salary including personal bonus has:",
        company.name_max_salary(), "\n")


if __name__ == "__main__":
    company_output(200)
