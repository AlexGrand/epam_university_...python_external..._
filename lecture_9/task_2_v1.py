def is_palindrome(string: str) -> str:
    if type(string) is not str:
        raise ValueError(f"'{string}' is not a str")

    from re import sub

    string = sub(r'[\W]', '', string.lower())

    return string == string[::-1]


if __name__ == "__main__":
    string = "Able, was I ere I saw Elba"
    string2 = "Doc, note: I dissent. A fast never prevents a fatness. "
    string2 += "I diet on cod"
    print(f"\nString @{string}@ is a palindrome: {is_palindrome(string)}\n")
    print(f"String @{string2}@ is a palindrome: {is_palindrome(string2)}")
