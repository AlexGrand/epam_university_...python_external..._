# [EPAM_University_...Python_External..._](/README.md)
### Lecture 9

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task 1:

![Task 1](static/Task_1.png)

Was implemented three different methods to swap quotes in string.<br>

Method `join` in `swap_quotes`:
```python
def swap_quotes(string: str) -> str:
    if type(string) is not str:
        raise ValueError(f"'{string}' is not a str")

    str_list = []
    for char in string:
        if char == "\"":
            char = "'"
        elif char == "'":
            char = "\""
        str_list.append(char)
    return ''.join(str_list)
```

Method with `translate` in `swap_quotes1`:
```python
def swap_quotes1(string: str) -> str:
    if type(string) is not str:
        raise ValueError(f"'{string}' is not a str")

    d_quotes = "'\""
    s_quotes = "\"'"
    swap = string.maketrans(d_quotes, s_quotes)
    return string.translate(swap)
```

Method `concatenating strings` in `swap_quotes2`:
```python
def swap_quotes2(string: str) -> str:
    if type(string) is not str:
        raise ValueError(f"'{string}' is not a str")

    new_str = ''
    for char in string:
        if char == "\"":
            char = "'"
        elif char == "'":
            char = "\""
        new_str += char
    return new_str
```

**OUTPUT**
![Output 1](static/output_1.png)

## Task 2:

![Task 2](static/Task_2.png)

Function `is_palindrome` uses regular expression to get all not whitespace chars and then checks if string is equal from both sides.
```python
def is_palindrome(string: str) -> str:
    if type(string) is not str:
        raise ValueError(f"'{string}' is not a str")

    from re import sub

    string = sub(r'[\W]', '', string.lower())

    return string == string[::-1]
```

**OUTPUT**
![Output 2](static/output_2.png)

## Task 3:

![Task 3](static/Task_3.png)

There was implemented two options to get shortest word. 
First one uses string concatenation and simple check of each element's length on each step looking smaller element than the previous smallest one.
```python
def get_shortest_word(s: str) -> str:
    if type(s) is not str:
        raise ValueError(f"'{s}' is not a str")

    shortest_w = s
    cur_w = ''

    for char in s:
        if char not in " \t\n\r\f\v":
            cur_w += char
        else:
            if len(cur_w) == 1:
                return cur_w
            elif len(cur_w) < len(shortest_w) and cur_w != '':
                shortest_w = cur_w
            cur_w = ''

    return shortest_w
```

Second one uses `append` method and return min element in list with key=len:
```python
def get_shortest_word1(s: str) -> str:
    if type(s) is not str:
        raise ValueError(f"'{s}' is not a str")

    words_list = []
    cur_word = ''

    for char in s:
        if char not in " \t\n\r\f\v":
            cur_word += char
        else:
            words_list.append(cur_word)
            cur_word = ''

    return min(words_list, key=len)
```

**OUTPUT**
![Output 3](static/output_3.png)

## Task 4:

![Task 4](static/Task_4.png)

All 4 functions use `set` data types

```python
def get_char_in_all(ls: list) -> set or None:
    if not ls:
        return None
    elif type(ls) is not list:
        raise ValueError(f"{ls} is not a list of strings")

    result = None
    for string in ls:
        if type(string) is not str:
            raise ValueError(f"{string} is not a str")

        if result is None:
            result = set(string)
        result = result & set(string)

    return result or None
```
```python
def get_char_in_one(ls: list) -> set or None:
    if not ls:
        return None
    elif type(ls) is not list:
        raise ValueError(f"{ls} is not a list of strings")

    result = set()
    for string in ls:
        if type(string) is not str:
            raise ValueError(f"{string} is not a str")

        result = result | set(string)

    return result or None
```
```python
def get_char_in_least_two(ls: list) -> set or None:
    if not ls:
        return None
    elif type(ls) is not list:
        raise ValueError(f"{ls} is not a list of strings")

    result = set()
    for i, string in enumerate(ls):
        if type(string) is not str:
            raise ValueError(f"{string} in {ls} is not a str")

        if i == 0:
            txt = ''.join(ls[1:])
        else:
            txt = ''.join(ls[:i] + ls[i+1:])

        new_set = set(string) & set(txt)
        result = result | new_set

    return result
```
```python
def get_unused_alpha_chars(ls: list) -> set or None:
    if not ls:
        return None
    elif type(ls) is not list:
        raise ValueError(f"{ls} is not a list of strings")

    from string import ascii_lowercase as _lowercased

    str_list = ''.join(ls)

    return set(_lowercased) - set(str_list)
```

**OUTPUT**
![Output 4](static/output_4.png)

I'm proud to introduce `get_char_in_least_two_2` which shows best results for the seeking chars that are at least in 2 strings. Method uses `join` and dict comprehension and finally shows keys that have more than 1 repeating char:
```python
def get_char_at_least_two_2(ls: list) -> list or None:
    if not ls:
        return None
    elif type(ls) is not list:
        raise ValueError(f"{ls} is not a list of strings")

    str_ls = ''.join(ls)
    chars_dict = {k: str_ls.count(k) for k in set(str_ls)}

    return [k for k in chars_dict if chars_dict[k] > 1]
```

Time complexity for this variant is reduced completely
**OUTPUT**
![Output 4_1](static/output_4_1.png)

## Task 5:

![Task 5](static/Task_5.png)

Function `count_letters` uses simple dict comprehension to find anc ount all letters in string
```python
def count_letters(string: str) -> dict or None:
    if not string:
        return None
    elif type(string) is not str:
        raise ValueError(f"{string} is not str")

    string = string.lower()
    return {k: string.count(k) for k in string if k.isalpha()}
```

**OUTPUT**
![Output 5](static/output_5.png)

### [Back to Main Page](/README.md)