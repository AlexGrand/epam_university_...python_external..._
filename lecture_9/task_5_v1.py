def count_letters(string: str) -> dict or None:
    if not string:
        return None
    elif type(string) is not str:
        raise ValueError(f"{string} is not str")

    string = string.lower()
    return {k: string.count(k) for k in string if k.isalpha()}


if __name__ == "__main__":
    string = "string:sample0;"
    msg = f"'{string}' has next letters in it:"
    print("\n", msg, "\n", count_letters(string))
