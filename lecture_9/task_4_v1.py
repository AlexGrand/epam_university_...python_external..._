def get_char_in_all(ls: list) -> set or None:
    if not ls:
        return None
    elif type(ls) is not list:
        raise ValueError(f"{ls} is not a list of strings")

    result = None
    for string in ls:
        if type(string) is not str:
            raise ValueError(f"{string} is not a str")

        if result is None:
            result = set(string)
        result = result & set(string)

    return result or None


def get_char_in_one(ls: list) -> set or None:
    if not ls:
        return None
    elif type(ls) is not list:
        raise ValueError(f"{ls} is not a list of strings")

    result = set()
    for string in ls:
        if type(string) is not str:
            raise ValueError(f"{string} is not a str")

        result = result | set(string)

    return result or None


def get_char_at_least_two(ls: list) -> set or None:
    if not ls:
        return None
    elif type(ls) is not list:
        raise ValueError(f"{ls} is not a list of strings")

    result = set()
    for i, string in enumerate(ls):
        if type(string) is not str:
            raise ValueError(f"{string} in {ls} is not a str")

        if i == 0:
            txt = ''.join(ls[1:])
        else:
            txt = ''.join(ls[:i] + ls[i+1:])

        new_set = set(string) & set(txt)
        result = result | new_set

    return result


def get_char_at_least_two_2(ls: list) -> set or None:
    if not ls:
        return None
    elif type(ls) is not list:
        raise ValueError(f"{ls} is not a list of strings")

    str_ls = ''.join(''.join(set(s)) for s in ls)
    chars_dict = {k: str_ls.count(k) for k in set(str_ls)}

    return [k for k in chars_dict if chars_dict[k] > 1]


def get_unused_alpha_chars(ls: list) -> set or None:
    if not ls:
        return None
    elif type(ls) is not list:
        raise ValueError(f"{ls} is not a list of strings")

    from string import ascii_lowercase as _lowercased

    str_list = ''.join(ls)

    return set(_lowercased) - set(str_list)


if __name__ == "__main__":
    test_strings = ["hello", "world", "python"]
    msg = f"In array of strings '{test_strings}'\n there are next"
    print(
        f'{msg} chars that appear in all strings:',
        sorted(get_char_in_all(test_strings)), '\n')
    print(
        f'{msg} chars that appear in at least one string:',
        sorted(get_char_in_one(test_strings)), '\n')
    print(
        f'{msg} chars that appear at least in two strings:',
        sorted(get_char_at_least_two(test_strings[::-1])), '\n')
    print(
        f'{msg} chars of alphabet, that were not used in any string:',
        sorted(get_unused_alpha_chars(test_strings)), '\n')
    import timeit

    print("\nChecking time complexity for get_char_at_least_two")
    ls_len = 100
    iterations = 10000
    test_strings *= ls_len
    print(f"get_char_at_least_two with list*{ls_len} and {iterations} iterations:")

    print(timeit.timeit(lambda: get_char_at_least_two(test_strings), number=iterations), "\n")
    print(f"get_char_at_least_two_2 with list*{ls_len} and {iterations} iterations:")
    print(timeit.timeit(lambda: get_char_at_least_two_2(test_strings), number=iterations))
