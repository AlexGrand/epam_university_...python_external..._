def get_shortest_word(s: str) -> str:
    if type(s) is not str:
        raise ValueError(f"'{s}' is not a str")

    shortest_w = s
    cur_w = ''

    for char in s:
        if char not in " \t\n\r\f\v":
            cur_w += char
        else:
            if len(cur_w) == 1:
                return cur_w
            elif len(cur_w) < len(shortest_w) and cur_w != '':
                shortest_w = cur_w
            cur_w = ''

    return shortest_w


def get_shortest_word1(s: str) -> str:
    if type(s) is not str:
        raise ValueError(f"'{s}' is not a str")

    words_list = []
    cur_word = ''

    for char in s:
        if char not in " \t\n\r\f\v":
            cur_word += char
        else:
            words_list.append(cur_word)
            cur_word = ''

    return min(words_list, key=len)


if __name__ == "__main__":
    string1 = 'Python is, simple and effective!'
    string2 = 'Any pythonistsa like namespaces a lot, a?'
    msg = 'Shortest word in string '

    print(f'{msg}@{string1}@ is: {get_shortest_word(string1)}')
    print(f'{msg}@{string2}@ is: {get_shortest_word(string2)}')

    import timeit

    first_f = timeit.timeit(lambda: get_shortest_word(string1), number=1000000)
    second_f = timeit.timeit(lambda: get_shortest_word1(string1), number=1000000)
    print("\n")
    print(f"func get_shortest_word is evaluated in:", first_f)
    print(f"func get_shortest_word1 is evaluated in:", second_f)
