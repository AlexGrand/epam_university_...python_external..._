def swap_quotes(string: str) -> str:
    if type(string) is not str:
        raise ValueError(f"'{string}' is not a str")

    str_list = []
    for char in string:
        if char == "\"":
            char = "'"
        elif char == "'":
            char = "\""
        str_list.append(char)
    return ''.join(str_list)


def swap_quotes1(string: str) -> str:
    if type(string) is not str:
        raise ValueError(f"'{string}' is not a str")

    d_quotes = "'\""
    s_quotes = "\"'"
    swap = string.maketrans(d_quotes, s_quotes)
    return string.translate(swap)


def swap_quotes2(string: str) -> str:
    if type(string) is not str:
        raise ValueError(f"'{string}' is not a str")

    new_str = ''
    for char in string:
        if char == "\"":
            char = "'"
        elif char == "'":
            char = "\""
        new_str += char
    return new_str


if __name__ == "__main__":
    string = "\nSam's birthday was'nt great at all.\" - said William"
    print("\nBefore:")
    print(string)
    print("\nAfter swap_quotes(string):")
    print(swap_quotes(string))

    print("\nCheck methods time complexity for join, trans, concat:\n")
    import timeit
    join, trans, concat = swap_quotes, swap_quotes1, swap_quotes2
    for method in [join, trans, concat]:
        print(timeit.timeit(lambda: method(string), number=100000))
