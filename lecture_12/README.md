# [EPAM_University_...Python_External..._](/README.md)
### Lecture 12

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task 1:

![Task 1](static/Task_1.png)

`ls` command outputs infromation about files in some kind of list. While `pwd` prints name of current working directory

**OUTPUT:**<br>
[![asciicast](https://asciinema.org/a/MdJETLO5bF7SlrYazejcHmq7S.svg)](https://asciinema.org/a/MdJETLO5bF7SlrYazejcHmq7S)

![Task_1.1](static/Task_1.1.png)

`mkdir` - creates directory\
`cd` - navigates to the directory\
`touch` - update  the  access and modification times of each FILE to the current. A FILE argument that does not exist is created empty\
`mv` - move (rename) files\
`cp` - copy files and directories\
`rm` - remove files or directories\
`rmdir` - remove empty directories

**OUTPUT:**<br>
[![asciicast](https://asciinema.org/a/wkfhnQs01KwFoNw3ZVDX7LLhW.svg)](https://asciinema.org/a/wkfhnQs01KwFoNw3ZVDX7LLhW)

![Task 1.2](static/Task_1.2.png)

`cat` - concatenates file to the standart output.
`less` is a non-standard pager (`more` is the standard one), used for viewing text, while `cat` is a standard utility, used for concatenating any type and number of data streams into one. `less` is preferable if you just wanna check some content of the huge file

**OUTPUT:**
[![asciicast](https://asciinema.org/a/nSkff5bJjyLKo32bjm1kzTDX5.svg)](https://asciinema.org/a/nSkff5bJjyLKo32bjm1kzTDX5)


## Task 2:

![Task 2](static/Task_2.png)

`ls -l .` - outputs longlisted format for the current directory\
`ln` - comman that makes links between files\
`ln -s` - creates symbolic link to the target file\
`rm test2.txt; ls -l` - removes `test2.txt` from now on `test3.txt`, that was a symbloic link to it - is broken. 

[![asciicast](https://asciinema.org/a/dgHczDfDRfroWw8Ln5fzHd126.svg)](https://asciinema.org/a/dgHczDfDRfroWw8Ln5fzHd126)

![Task 2.1](static/Task_2.1.png)

[![asciicast](https://asciinema.org/a/wZroYzZj2FZyXqOJm1n5yaxmC.svg)](https://asciinema.org/a/wZroYzZj2FZyXqOJm1n5yaxmC)


# [BACK TO MAIN](/README.md)