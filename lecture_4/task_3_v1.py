def calculate_Fibonacci(num: int) -> (list, int):
    """calculates Fibonacci sequence for number and returns
       a tuple (Fibonacci_seqence, sum_of_elements)
    """
    sequence = []

    for i in range(0, num):
        if i < 2:
            sequence.append(int(i != 0))
            continue
        sequence.append(sequence[i-2] + sequence[i-1])

    return (sequence, sum(sequence))


if __name__ == "__main__":
    number = int(input("Please, enter the length of sequence: "))
    fibonacci_result = calculate_Fibonacci(number)

    print(f"Fibonacci sequence is {str(fibonacci_result[0])}")
    print(f"Sum of elements in Fibonacci sequence is {fibonacci_result[1]}")
