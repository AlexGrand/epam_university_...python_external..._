def dec_to_bin(num: int) -> (int, int):
    def convert_to_bin(num: int) -> str:
        div, mod = divmod(num, 2)

        if not div:
            return str(mod)
        return str(mod) + str(convert_to_bin(div))

    bn = convert_to_bin(num)[::-1]
    return int(bn), bn.count('1')


if __name__ == "__main__":
    number = int(input("Your number: "))
    if number >= 1:
        bin_result = dec_to_bin(number)
        print(
            f"Number is {number}, binary representation is {bin_result[0]},"
            + f" sum is {bin_result[1]}"
        )
    else:
        print("Type in positive integer!")
