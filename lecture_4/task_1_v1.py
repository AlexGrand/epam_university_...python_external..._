def transpose_matrix(matrix: list) -> list:
    new_matrix = []

    for col in range(0, len(matrix[0])):
        new_matrix.append([i[col] for i in matrix])

    return new_matrix


if __name__ == '__main__':
    matrix = [
        [0, 1, 9],
        [9, 8, 5],
        [7, 3, 1],
        [3, 4, 0],
    ]

    print(
        "Original matrix is:",
        ''.join([f'{i}\n' for i in matrix]),
        sep='\n'
        )
    print(
        "Transposed matrix is: ",
        ''.join([f'{i}\n' for i in transpose_matrix(matrix)]),
        sep="\n"
        )
