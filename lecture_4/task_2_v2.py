def calculate_factorial(num: int) -> int:
    return int(num == 1) or num * calculate_factorial(num - 1)


if __name__ == "__main__":
    num = int(input('Please, enter the number: '))
    if num <= 0:
        print('Please type in a positive integer!')
    else:
        print(f"Factorial of {num} is {calculate_factorial(num)}")
