# [EPAM_University_...Python_External..._](/README.md)
### Lecture 4

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task 1:
- [x] Transpose a matrix
![Transpose a matrix](static/Task_1.png)

### Version 1 ([Task_1_v1.py](/lecture_4/task_1_v1.py))
Uses `List comprehension` inside of `for` cycle.
```python
def transpose_matrix(matrix: list) -> list:
    new_matrix = []

    for col in range(0, len(matrix[0])):
        new_matrix.append([i[col] for i in matrix])

    return new_matrix
```

## Task 2:
- [x] Factorial
![Factorial](static/Task_2.png)

### Version 1 ([Task_2_v1.py](/lecture_4/task_2_v2.py))
Uses recursion call for a factorial
```python
def calculate_factorial(num: int) -> int:
    return int(num == 1) or num * calculate_factorial(num - 1)
```

## Task 3:
- [x] Fibonacci sequence
![Fibonacci sequence](static/Task_3.png)

### Version 1 ([Task_3_v1.py](/lecture_4/task_3_v1.py))
Uses a `for` cycle to append fibonacci numbers to the `sequence`. If number is less than 2 users Bool convertation.
```python
def calculate_Fibonacci(num: int) -> (list, int):
    """calculates Fibonacci sequence for number and returns
       a tuple (Fibonacci_seqence, sum_of_elements)
    """
    sequence = []

    for i in range(0, num):
        if i < 2:
            sequence.append(int(i != 0))
            continue
        sequence.append(sequence[i-2] + sequence[i-1])

    return (sequence, sum(sequence))
```

## Task 4:
- [x] Binary representation
![Binary representation](static/Task_4.png)

### Version 1 ([Task_4_v1.py](/lecture_4/task_4_v1.py))
Uses recursion and `divmod` method to get the binary num in `str`, than converts it backward and counts '1' in the string.
```python
def dec_to_bin(num: int) -> (int, int):
    def convert_to_bin(num: int) -> str:
        div, mod = divmod(num, 2)

        if not div:
            return str(mod)
        return str(mod) + str(convert_to_bin(div))

    bn = convert_to_bin(num)[::-1]
    return int(bn), bn.count('1')

```


### [Back to Main Page](/README.md)