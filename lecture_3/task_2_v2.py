def print_FizzBuzz(num: int) -> str:
    try:
        num = int(num)
    except ValueError:
        return 'Not an integer!'

    cases = [
        f'{num}', 'Buzz', 'Fizz', 'FizzBuzz',
        f'{num} is out of range [1, 100]'
        ]

    results = cases[
        2*(num % 3 == 0) + (num % 5 == 0)
        or 4*(num < 1 or num > 100)]

    return results


if __name__ == '__main__':
    number = input('Enter the number: ')
    print(print_FizzBuzz(number))
