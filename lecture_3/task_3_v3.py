CARDS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']


def play(first_card: str, second_card: str) -> str:
    if 'joker' in (first_card.lower(), second_card.lower()):
        return 'Do not cheat!'
    elif first_card.upper() not in CARDS or second_card.upper() not in CARDS:
        return 'Choose right card!'

    def get_a_score(card: str) -> int:
        if card in CARDS[:CARDS.index('10')]:
            return int(card)
        else:
            return int(card.upper() == CARDS[-1])

    first_card, second_card = get_a_score(first_card), get_a_score(second_card)

    if first_card + second_card > 9:
        return str(first_card + second_card - 10)

    return str(first_card + second_card)


if __name__ == '__main__':
    first_card = str(input('Play first card: '))
    second_card = str(input('Play second card: '))

    print(
        'Your result:', play(first_card, second_card)
    )
