def print_FizzBuzz(num: int) -> str:
    try:
        num = int(num)
    except ValueError:
        return 'Not an integer!'

    cases = {
            f'{num} is out of range [1, 100]': num <= 0 or num > 100,
            'FizzBuzz': num % 15 == 0,
            'Fizz': num % 3 == 0,
            'Buzz': num % 5 == 0,
            f'{num}': num
        }

    return [key for key, val in cases.items() if val][0]


if __name__ == '__main__':
    number = input('Enter the number: ')
    print(print_FizzBuzz(number))
