CARDS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']


def play(first_card: str, second_card: str) -> str:
    first_card, second_card = first_card.lower(), second_card.lower()

    if 'joker' in (first_card, second_card):
        return 'Do not cheat!'
    elif first_card.upper() not in CARDS or second_card.upper() not in CARDS:
        return 'Choose right card!'

    def check_card(card: str) -> int:
        if card in '10jqk':
            return 0
        elif card == 'a':
            return 1
        else:
            return int(card)

    first_card = check_card(first_card)
    second_card = check_card(second_card)
    total = first_card + second_card

    if total > 9:
        return str(total - 10)

    return str(first_card + second_card)


if __name__ == '__main__':
    first_card = str(input('Play first card: '))
    second_card = str(input('Play second card: '))

    print(
        'Your result:', play(first_card, second_card)
    )
