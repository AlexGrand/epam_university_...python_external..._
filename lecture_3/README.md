# [EPAM_University_...Python_External..._](/README.md)
### Lecture 2

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task 1:
- [x] Write a program that determines whether the Point A(x,y) is in the shaded area or not.

![Image Shape](static/Task_1.png)

### Version 1 ([Task_1_v1.py](/lecture_3/task_1_v1.py))
```python
def validate_point(x: float, y: float) -> bool:
    return y >= abs(x) and y <= 1
```
**RESULT:**<br> 
> $> Enter x: 0.5<br>
> $> Enter y: 0.7<br>
> $> Is point in shadow area:  True

### Version 2 ([Task_1.py](r/lecture_3/task_1.py))
In case the shape will be changed user can add new conditions into `cases` dict.
```python
def validate_point(x: float, y: float) -> bool:
    cases = {
        'a': abs(x) <= abs(y),
        'b': y >= 0,
        'c': y <= 1,
        'd': abs(x) <= 1,
    }
    return False not in cases.values()
```
**RESULT:**<br> 
> $> Enter x: 0.5<br>
> $> Enter y: 0.7<br>
> $> Is point in shadow area:  True

## Task 2
- [x] Write a program that prints the input number from 1 to 100. But for multiple of three print "Fizz" instead of the number and for the multiples of five print "Buzz". For numbers which are multiples of both three and five print "FizzBuzz".

![Image Shape](static/Task_2.png)

### Version 1 ([Task_2.py](/lecture_3/task_2.py))
Uses `cases` dict to hold all possible conditions. Then uses list comprehension to loop through list of checks.
```python
def print_FizzBuzz(num: int) -> str:
    try:
        num = int(num)
    except ValueError:
        return 'Not an integer!'

    cases = {
            f'{num} is out of range [1, 100]': num <= 0 or num > 100,
            'FizzBuzz': num % 15 == 0,
            'Fizz': num % 3 == 0,
            'Buzz': num % 5 == 0,
            f'{num}': num
        }

    return [key for key, val in cases.items() if val][0]
```

### Version 2 ([task_2_v2.py](/lecture_3/task_2_v2.py))
```python
def print_FizzBuzz(num: int) -> str:
    try:
        num = int(num)
    except ValueError:
        return 'Not an integer!'

    cases = [
        f'{num}', 'Buzz', 'Fizz', 'FizzBuzz',
        f'{num} is out of range [1, 100]'
        ]

    results = cases[
        2*(num % 3 == 0) + (num % 5 == 0)
        or 4*(num < 1 or num > 100)]

    return results
```

## Task 3
- [x] Simulate the one round of play for baccarat game:
Rules:
cards have a point value:
the 2 through 9 cards in each suit are worth face value (in points);
the 10, jack, queen and king have no point value (i.e. are worth zero);
aces are worth 1 point.
Sum the values of cards. If total is more than 9 reduce 10 from result. E.g. 5 + 9 = 14, 14 - 10 = 4, 4 is the result

![Image Shape](static/Task_3.png)

### Version 1 ([task_3_v1.py](/lecture_3/task_3_v1.py))
Using inner `check_card` func to check each card.
```python
def play(first_card: str, second_card: str) -> str:
    first_card, second_card = first_card.lower(), second_card.lower()

    if 'joker' in (first_card, second_card):
        return 'Do not cheat!'
    elif first_card.upper() not in CARDS or second_card.upper() not in CARDS:
        return 'Choose right card!'

    def check_card(card: str) -> int:
        if card in '10jqk':
            return 0
        elif card == 'a':
            return 1
        else:
            return int(card)

    first_card = check_card(first_card)
    second_card = check_card(second_card)
    total = first_card + second_card

    if total > 9:
        return str(total - 10)

    return str(first_card + second_card)
```

### Version 2 ([task_3_v2.py](/lecture_3/task_3_v2.py))
In this case using `for` loop to check conditions for each condition.
```python
def play(first_card: str, second_card: str) -> str:
    card_1, card_2 = first_card.lower(), second_card.lower()

    if 'joker' in (card_1, card_2):
        return 'Do not cheat!'
    elif card_1.upper() not in CARDS or card_2.upper() not in CARDS:
        return 'Choose right card!'

    cases = {
        '10jqk': lambda x: int(x not in '10jqk'),
        'a': lambda x: int(x == 'a'),
        '23456789': lambda x: int(x)
    }

    for key, val in cases.items():
        if card_1 in key:
            first_card = val(card_1)
        if card_2 in key:
            second_card = val(card_2)

    if first_card + second_card > 9:
        return str(first_card + second_card - 10)

    return str(first_card + second_card)
```
### Version 3 ([task_3_v2.py](/lecture_3/task_3_v3.py))
In this case using slicing. `get_a_score` func reverts result into `True` or `False` that is then converted into 1 or 0.
```python
def play(first_card: str, second_card: str) -> str:
    if 'joker' in (first_card.lower(), second_card.lower()):
        return 'Do not cheat!'
    elif first_card.upper() not in CARDS or second_card.upper() not in CARDS:
        return 'Choose right card!'

    def get_a_score(card):
        if card in CARDS[:CARDS.index('10')]:
            return int(card)
        else:
            return int(card == CARDS[-1])

    first_card, second_card = get_a_score(first_card), get_a_score(second_card)

    if first_card + second_card > 9:
        return str(first_card + second_card - 10)

    return str(first_card + second_card)
```

### [Back to Main Page](/README.md)
