CARDS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']


def play(first_card: str, second_card: str) -> str:
    card_1, card_2 = first_card.lower(), second_card.lower()

    if 'joker' in (card_1, card_2):
        return 'Do not cheat!'
    elif card_1.upper() not in CARDS or card_2.upper() not in CARDS:
        return 'Choose right card!'

    cases = {
        '10jqk': lambda x: int(x not in '10jqk'),
        'a': lambda x: int(x == 'a'),
        '23456789': lambda x: int(x)
    }

    for key, val in cases.items():
        if card_1 in key:
            first_card = val(card_1)
        if card_2 in key:
            second_card = val(card_2)

    if first_card + second_card > 9:
        return str(first_card + second_card - 10)

    return str(first_card + second_card)


if __name__ == '__main__':
    first_card = str(input('Play first card: '))
    second_card = str(input('Play second card: '))

    print(
        'Your result:', play(first_card, second_card)
    )
