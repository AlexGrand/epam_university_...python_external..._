def validate_point(x: float, y: float) -> bool:
    cases = {
        'a': abs(x) <= abs(y),
        'b': y >= 0,
        'c': y <= 1,
        'd': abs(x) <= 1,
    }
    return False not in cases.values()


if __name__ == '__main__':
    x = float(input('Enter x: '))
    y = float(input('Enter y: '))

    print('Is point in shadow area: ', validate_point(x, y))
