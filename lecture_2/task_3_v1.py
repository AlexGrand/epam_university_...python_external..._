def swap_words(string: str):
    str_ls = string.split(' ')
    word_1 = 'reasonable'
    word_2 = 'unreasonable'

    word_1_i = str_ls.index(word_1)
    word_2_i = str_ls.index(word_2)

    str_ls[word_1_i], str_ls[word_2_i] = word_2, word_1

    string = ' '.join(str_ls)

    return string


if __name__ == "__main__":
    text = "The reasonable man adopts himself to the world; "
    text += "the unreasonable one persists in trying to "
    text += "adopt the world to himself."

    print(swap_words(text))
