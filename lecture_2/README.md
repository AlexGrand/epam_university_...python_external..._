# [EPAM_University_...Python_External..._](/README.md)
### Lecture 2

**Student:** Oleksandr Maistrenko<br>
**email:** namelost@protonmail.com<br>

## Task List:
### [Task 1](/lecture_2/README.md#task-1-1)
- [x] return the count of negative numbers in next list [4, -9, 8, -11, 8]
- [x] don't use conditionals or loops

### [Task 2](/lecture_2/README.md#task-2-1)
- [x] You have 5 best players according API rankings. Set the first place player (at the front of the list) to last place and vice versa:<br>
```python
players=[
            'Ashleigh Barty', 'Simona Halep', 'Naomi Osaka',
            'Karolina Pliskova', 'Elina Svitolina',
        ]
```

### [Task 3](/lecture_2/README.md#task-3-1)
- [x] Swap words 'reasonable' and 'unreasonable' in quote:<br> 
> The reasonable man adopts himself to the world; the unreasonable one persists in trying to adopt the world to himself.
- [x] Don't use `<string>.replace()` function or similar.


## Completed tasks:
### Task 1
#### [v1](/lecture_2/task_1_v1.py) ([Back to task](/lecture_2/README.md#task-1))
Making a string from the list and counting '-' chars:
```python
def count_negatives(numbers: list) -> int:
    return f'{numbers}.count('-')
```
**RESULT:**
> $>>> There are 2 negative numbers in list [4, -9, 8, -11, 8]

[Source code: task_1_v1.py](/lecture_2/task_1_v1.py)

#### [v2](/lecture_2/task_1_v2.py) ([Back to task](/lecture_2/README.md#task-1))

Filtering numbers list and counting amount of returned elements.
```python
def count_negatives(numbers: list) -> int:
    return len(list(filter(lambda num: num < 0, numbers)))
```
**RESULT:**
> $>>> There are 2 negative numbers in list [4, -9, 8, -11, 8]

[Source code: task_1_v2.py](/lecture_2/task_1_v2.py)

#### [v3](/master/lecture_2/task_1_v3.py) ([Back to task](/master/lecture_2/README.md#task-1))

Inner recursive function `iterate_nums` fills new list `ls` with `True/False` elements depending on positive or negative elements it gets from the `numbers` list. `count_negatives` function returns the count of `True` elements in the new list. Uses Iterator.

```python
def count_negatives(numbers: list) -> int:
    itr = iter(numbers)
    ls = []

    def iterate_nums():
        nonlocal ls
        try:
            num = next(itr)
            ls += [num < 0 or False]
            iterate_nums()
        except StopIteration:
            pass

    iterate_nums()

    return ls.count(True)
```
**RESULT:**
> $>>> There are 2 negative numbers in list [4, -9, 8, -11, 8]

[Source code: task_1_v3.py](/lecture_2/task_1_v3.py)


### Task 2
#### [v1](/task_2_v1.py) ([Back to task](/lecture_2/README.md#task-2))

Function `change_positions` takes players list and return new list with first and last elements swapped between.
```python
def change_positions(players: list):
    players = [players[-1]] + players[1:-1] + [players[0]]
    print(players)
```
**RESULT:**
> $>>> ['Elina Svitolina', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Ashleigh Barty']

[Source code: task_2_v1.py](/lecture_2/task_2_v1.py)

#### [v2](/lecture_2/task_2_v2.py) ([Back to task](/lecture_2/README.md#task-2))
Function `change_positions` takes first and last element of the `players` list and deserializes  tuple value from last and first element.
```python
def change_positions(players: list):
    players[0], players[-1] = players[-1], players[0]
    print(players)
```
**RESULT:**
> $>>> ['Elina Svitolina', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Ashleigh Barty']

[Source code: task_2_v2.py](/lecture_2/task_2_v2.py)

### Task 3
#### [v1](/lecture_2/task_3_v1.py) ([Back to task](/lecture_2/README.md#task-3))
Function `swap_words` takes a string and splits it onto single words; finds element's index for `reasonable/unreasonable` words and swaps them between. Then it `return` a new string joined from the words list.
```python
def swap_words(string: str):
    str_ls = string.split(' ')
    word_1 = 'reasonable'
    word_2 = 'unreasonable'

    word_1_i = str_ls.index(word_1)
    word_2_i = str_ls.index(word_2)

    str_ls[word_1_i], str_ls[word_2_i] = word_2, word_1

    string = ' '.join(str_ls)

    return string
```
**RESULT:**
> $>>> The unreasonable man adopts himself to the world; the reasonable one persists in trying to adopt the world to himself.

[Source code: task_3_v1.py](/lecture_2/task_3_v1.py)

#### [v2](/lecture_2/task_3_v2.py) ([Back to task](r/lecture_2/README.md#task-3))
Function `swap_words` uses slicing method of the string to slice original string into pieces without `reasonable/unreasonable` words. Finally it concatenates slices and swapped words into a new string.
```python
def swap_words(string: str):
    word_1 = 'reasonable'
    word_2 = 'unreasonable'

    word_1_i = string.find(word_1)
    word_2_i = string.find(word_2)

    slice_1 = string[:word_1_i]
    slice_2 = string[word_1_i + len(word_1):word_2_i]
    slice_3 = string[word_2_i + len(word_2): len(string)]

    string = slice_1 + word_2 + slice_2 + word_1 + slice_3
    return string
```
> $>>> The unreasonable man adopts himself to the world; the reasonable one persists in trying to adopt the world to himself.

[Source code: task_3_v2.py](/lecture_2/task_3_v2.py)

### [Back to Main Page](/master/README.md)
