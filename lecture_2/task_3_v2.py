def swap_words(string: str):
    word_1 = 'reasonable'
    word_2 = 'unreasonable'

    word_1_i = string.find(word_1)
    word_2_i = string.find(word_2)

    slice_1 = string[:word_1_i]
    slice_2 = string[word_1_i + len(word_1):word_2_i]
    slice_3 = string[word_2_i + len(word_2): len(string)]

    string = slice_1 + word_2 + slice_2 + word_1 + slice_3
    return string


if __name__ == "__main__":
    text = "The reasonable man adopts himself to the world; "
    text += "the unreasonable one persists in trying to "
    text += "adopt the world to himself."

    print(swap_words(text))
