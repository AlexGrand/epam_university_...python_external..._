def count_negatives(numbers: list) -> int:
    itr = iter(numbers)
    ls = []

    def iterate_nums():
        nonlocal ls
        try:
            num = next(itr)
            ls += [num < 0 or False]
            iterate_nums()
        except StopIteration:
            pass

    iterate_nums()

    return ls.count(True)


if __name__ == "__main__":
    input_numbers = [4, -9, 8, -11, 8]
    print(
        f"There are {count_negatives(input_numbers)}"
        + f" negative numbers in list {input_numbers}"
        )